# -*- coding: utf-8 -*-
"""
Created on Mon Oct  5 17:20:15 2020

@author: vania - - University of Coimbra / Eramus MC
"""

import numpy as np
import math
import App.OctLayers as oct
import warnings

warnings.filterwarnings(action='ignore', category=RuntimeWarning)

def contrastMap(img,n):
    """
    Computes contrast map of a 2d matrix with duplicate borders padding
    
    Parameters
    ----------
    img : Matrix with the original image
    n : Size of the square element (n x n)

    Returns
    -------
    k : Contrast map 

    """
    warnings.filterwarnings("ignore")
    #Check if the element number can be applied to a given matrix/image 
    if (n%2==0 or n==1):
        print('The size of the element must an odd number (>1)')
        
    #Duplicate borders padding
    pad=np.pad(img,pad_width=int((n-1)/2),mode='edge')
    
    #Compute spacial contrast map
    shape=img.shape
    u=(img*0).astype(np.float32)
    k=(img*0).astype(np.float32)
    s=np.zeros((n,n))
    
    for i in range(shape[0]):
        for j in range(shape[1]):
            u[i,j]=1/(n**2)*(np.sum(pad[i:i+n,j:j+n])) # u is the mean intensity value in the element with a central pixel in the coordinates (i, j)
            s=(pad[i:i+n,j:j+n]-u[i,j])**2 # s is (I(x,y)-u(i,j))^2 for each pixel in the element nxn
            k[i,j]=math.sqrt(1/(n**2)*np.sum(s))/u[i,j] # k is the contrast value for the element centered in (i, j)
            
    return k

def contrastMap3d(img,n):
    """
    Computes contrast map of a 3d volume with duplicate borders padding

    Parameters
    ----------
    img : 3D Matrix with the original volume
    n : Size of the cubic element (n x n x n)

    Returns
    -------
    contrast : Contrast map

    """
    if (n%2==0 or n==1):
        print('The size of the element must an odd number (>1)')
        
    #Duplicate borders padding
    pad=np.pad(img,pad_width=int((n-1)/2),mode='edge')
    
    #Compute spacial contrast map
    shape=img.shape
    u=(img*0).astype(np.float32)
    contrast=(img*0).astype(np.float32)
    s=np.zeros((n,n,n))
    
    for i in range(shape[0]):
           for j in range(shape[1]):
               for k in range(shape[2]):
                    u[i,j,k]=1/(n**2)*(np.sum(pad[i:i+n,j:j+n,k:k+n])) # u is the mean intensity value in the element with a central pixel in the coordinates (i, j, k)
                    s=(pad[i:i+n,j:j+n,k:k+n]-u[i,j,k])**2 # s is (I(x,y,z)-u(i,j,k))^2 for each pixel in the element nxn
                    contrast[i,j,k]=math.sqrt(1/(n**2)*np.sum(s))/u[i,j,k] # k is the contrast value for the element centered in (i, j, k)
                    
    return contrast



# -*- coding: utf-8 -*-
"""
Created on Wed Mar 10 22:29:09 2021

@author: vania - University of Coimbra / Eramus MC
"""

import numpy as np
from skimage.feature import graycomatrix, graycoprops
from scipy.stats import entropy
import App.OctLayers as oct

def SGLDM(volume,DISTANCES):
    """
    
    Parameters
    ----------
    I : Image
    DISTANCES : Array with the distances to use in the SGLDM calculation

    Returns
    -------
    prop : List of arrays for each propertity: Correlation, Contrast, Homogeneity, Energy
    Each array in the list has size (len(d),len(a)) contains the property value for each distance d and angle a
    """


    ANGLES = [0., np.pi/4., np.pi/2., 3.*np.pi/4.]
    level=256
    #Pre-implemented properties
    properties = ["correlation", "contrast", "homogeneity", "energy"]
    #New array of properties 
    properties2 = ["correlation", "contrast", "homogeneity", "energy", "Inertia", "Entropy"]
    angles_degrees=[0,45,90,135]
    
    

    prop_values=np.zeros((len(properties2),len(DISTANCES),len(ANGLES)))
    
    count=0
    for n in range(volume.shape[0]):
        
        I=volume[n]
        #Calculate SGLDM matrix
        #GLCM[i,j,d,theta] is the probability that grey-level j occurs at a distance d and at an angle theta from grey-level i
        glcm = graycomatrix(I, distances=DISTANCES, angles=ANGLES, levels=level+1, symmetric=False, normed=False)
        
        glcm=glcm[1:,1:] #eliminate the intensity 0 (background)
        
        if (np.sum(glcm)!=0):
            count+=1
            glcm=glcm/np.sum(glcm)
            glcm+=0.0001 #Make sure no value is zero
            
            # prop- list of properties in array properties
            # each array in prop represents the property for each angle(lines) and distance(column)
            prop = [graycoprops(glcm, properties[i]) for i in range(len(properties))]
            prop = np.array(prop)
            
            #Calculate inertia and entropy
            inertia=0
            inertias_entropies=np.zeros((2,len(DISTANCES),len(ANGLES)))
            
            
            for a in range(len(ANGLES)):
                for d in range(len(DISTANCES)):
                    for i in range(level):
                        for j in range(level):
                            inertia += ((i-j)**2) * glcm[i,j,d,a]
                    inertias_entropies[0,d,a]=inertia
                    inertias_entropies[1,d,a]=np.nansum(entropy(glcm[:,:,d,a]))
                    
            prop=np.concatenate((prop,inertias_entropies))
            prop_values+=prop #sum the values of each b-scan
    
    prop_values/=count #divide by the number of b-scans to have the mean
    
    columns=[]
    values=[]
    
    for p in range(len(properties2)):
        for d in range(len(DISTANCES)):
            for a in range(len(angles_degrees)):
                columns.append('SGLDM_'+properties2[p]+'_'+str(angles_degrees[a])+'_'+str(DISTANCES[d]))
                values.append(prop_values[p,d,a])
            

    return columns, values, glcm
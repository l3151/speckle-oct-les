# -*- coding: utf-8 -*-
"""
@author: vania - University of Coimbra / Eramus MC
Scrip to create table with speckle features from OCT volumes

Update on 09/05/2023 to log-inverse transform
"""

from SGLDM import SGLDM
from contrastMap import *
from pdfs import pdfs
from fourierDomain import fourierFeatures3D
import App.OctLayers as oct
import numpy as np
from scipy.stats import skew, kurtosis
import time
import pandas as pd
import os
import warnings
from joblib import Parallel, delayed

warnings.filterwarnings("ignore")

def createFeatures(f):    
    print(f)
    volume=list(filter(lambda x: os.path.isdir(os.path.join(rootdir+'\\'+folders[f], x)), os.listdir(rootdir+'\\'+folders[f])))
    filename = volume[0]
    filepath = rootdir+'\\'+folders[f]


    #Import volume
    fname = filepath+'\\'+filename+'.img'
    fname_layers = filepath+'\\'+filename+'\\'+filename+'_Surfaces_Iowa.xml'
    
    #Import layer's mask using 0 - background, 1 - RNFL, 2 - GCL, 3 - IPL, 4 - INL, 5 - OPL, 6 - ONL, 7 - IS/OS, 8 - OuterSegment, 9 - OPR and 10 - RPE
    mask=np.load(filepath+'\\'+filename+'\\'+filename+'_octmask.npy')
    
    #Create matrix with OCT volume
    data_oct = oct.OctLayers(filename=fname_layers,
                             raw_filename=fname)

    V_aux = np.power(10,data_oct.octdata/255)
    V2 = (V_aux-np.min(V_aux))/(10-np.min(V_aux))

    # Create contrast map of the volume with 3x3 neighbourhood
    contrast = contrastMap3d(V2,3)
    print("Contrast computation completed.")

    #Create feature and column names for each layer 
    layers=["RNFL", "GCL","IPL","INL","OPL","ONL","IS/OS","OuterSegment","OPR","RPE"]
    
    #Create columns names and values for each feature
    columns=["ID"]
    values=[filepath[-3:]] #Patient ID
    
    
    for i in range(len(layers)):

        #Statistic measures
        columns.append("Mean_"+layers[i])
        values.append(np.nanmean(V2[mask==i+1]))
        columns.append("Sd_"+layers[i])
        values.append(np.nanstd(V2[mask==i+1]))
        columns.append("Skewness_"+layers[i])
        values.append(skew(V2[mask==i+1]))
        columns.append("Kurtosis_"+layers[i])
        values.append(kurtosis(V2[mask==i+1]))
        
        # Contrast map features
        columns.append("ContrastMap_mean_"+layers[i])
        values.append(np.nanmean(contrast[mask==i+1]))
        columns.append("ContrastMap_sd_"+layers[i])
        values.append(np.nanstd(contrast[mask==i+1]))
        
        # Statistical distribution features
        layer=V2[mask==i+1]
        c, v = pdfs(layer)
        columns+=[j+'_'+layers[i] for j in c]
        values+=v
        
        # Fourier Domain features
        volume=V2.copy()
        volume[mask!=i+1]=0
        c, v = fourierFeatures3D(volume,4)
        columns+=[j+'_'+layers[i] for j in c]
        values+=v.tolist()
        
        # SGLDM features
        V = (V2 * 255).round().astype(np.uint8)
        volume=V.copy()
        volume=volume+1 #make sure only the background is zero
        volume[mask!=i+1]=0
        c, v, sgl = SGLDM(volume, distances)
        columns+=[j+'_'+layers[i] for j in c]
        values+=v
    
    #Create data frame   
    df = pd.DataFrame(values,columns) 
    df=df.transpose()
    
    #Set ID as index of data frame
    df=df.set_index("ID")
    df.to_csv(filepath+"\\"+filename+"\\"+filename+"_SpeckleFeatures_log_inverse.csv")
 
warnings.filterwarnings(action='ignore', category=RuntimeWarning)

#Count the time
start = time.time()

#Distances for calculating SGLDM
distances=[1]

#Root directory to files
rootdir=os.getcwd() + '\\Sample'

#Get all folder in root directory    
folders = list(filter(lambda x: os.path.isdir(os.path.join(rootdir, x)), os.listdir(rootdir)))

#Create features

for f in range(len(folders)):
    createFeatures(f)


end = time.time()
print(f"Time: {round((end - start)/60)} minutes")
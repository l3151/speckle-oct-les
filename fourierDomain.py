# -*- coding: utf-8 -*-
"""
Created on Mon Mar 22 15:14:27 2021

@author: vania
"""


import App.OctLayers as oct
import numpy as np



def fourierFeatures2D(I,k):
    """

    Parameters
    ----------
    I : 2D Image
    k : Number of spatial magnitude regions to divide

    Returns
    -------
    columns : Names of columns
    values : Feature values for each columns in columns

    """

    f = np.fft.fft2(I)
    fshift = np.fft.fftshift(f)
    
    N=fshift.shape[0]
    M=fshift.shape[1]
      
        
    regions= np.empty((k, 0)).tolist()
    
    for i in range(k):
        
        pixels=(fshift[int((i+1)*N/(k*2)):int(N-((i+1)*N/(k*2))),int(i*M/(k*2)):int((i+1)*M/(k*2))]).flatten() 
        pixels=np.concatenate((pixels,(fshift[int((i+1)*N/(k*2)):int(N-((i+1)*N/(k*2))),int(M-((i+1)*M/(k*2))):int(M-(i*M/(k*2)))]).flatten())) 
        pixels=np.concatenate((pixels,(fshift[int(i*N/(k*2)):int((i+1)*N/(k*2)),int(i*M/(k*2)):int(M-(i*M/(k*2)))]).flatten()))
        pixels=np.concatenate((pixels,(fshift[int(N-((i+1)*N/(k*2))):int(N-(i*N/(k*2))),int(i*M/(k*2)):int(M-(i*M/(k*2)))].flatten() )))
    
        regions[i]=pixels
        
    values= np.zeros((k))
    columns=[]
    
    total_magnitude=np.sum(abs(fshift))
    
    for i in range(k):
        values[i]=np.sum(abs(regions[i]))/total_magnitude
        columns.append("Frequency_"+str(i))
        
    return columns,values


def fourierFeatures(volume,k):
    """

    Parameters
    ----------
    I : 1D vector
    k : Number of spatial magnitude regions to divide

    Returns
    -------
    columns : Names of columns
    values : Feature values for each columns in columns

    """
    #FFT
    f = np.fft.fft(volume)
    fshift = np.fft.fftshift(f)
    
    #Consider only the positive frequencies (Fourier space is simetrical)
    F=fshift[int(len(fshift)/2+1):]
    n=len(F)
        
    
    #Divide Fourier space into k regions
    regions= np.empty((k, 0)).tolist()
    for i in range(k):
      regions[i]=F[int(i*(n/4)):int((i+1)*(n/4))]
    
      
    #Calculate the percentage of contribution of each fourier region and create column names
    values= np.zeros((k))
    columns=[]
    
    total_magnitude=np.sum(abs(F))
    
    for i in range(k):
        values[i]=np.sum(abs(regions[i]))/total_magnitude
        columns.append(f"Frequency_{i+1}")
        
    return columns,values

def fourierFeatures3D(volume,k):
    """

    Parameters
    ----------
    I : 3D volume
    k : Number of spatial magnitude regions to divide

    Returns
    -------
    columns : Names of columns
    values : Feature values for each columns in columns

    """
    #FFT
    f = np.fft.fftn(volume)
    fshift = np.fft.fftshift(f)
    
    N=fshift.shape[1]
    M=fshift.shape[2]
    O=fshift.shape[0]
      
        
    regions= np.empty((k, 0)).tolist()
    
    for i in range(k):
        pixels=(fshift[int(i*O/(k*2)):int(O-(i*O/(k*2))),int((i+1)*N/(k*2)):int(N-((i+1)*N/(k*2))),int(i*M/(k*2)):int((i+1)*M/(k*2))]).flatten()
        pixels=np.concatenate((pixels,(fshift[int(i*O/(k*2)):int(O-(i*O/(k*2))),int((i+1)*N/(k*2)):int(N-((i+1)*N/(k*2))),int(M-((i+1)*M/(k*2))):int(M-(i*M/(k*2)))]).flatten()))
        pixels=np.concatenate((pixels,(fshift[int(i*O/(k*2)):int(O-(i*O/(k*2))),int(i*N/(k*2)):int((i+1)*N/(k*2)),int(i*M/(k*2)):int(M-(i*M/(k*2)))]).flatten()))
        pixels=np.concatenate((pixels,(fshift[int(i*O/(k*2)):int(O-(i*O/(k*2))),int(N-((i+1)*N/(k*2))):int(N-(i*N/(k*2))),int(i*M/(k*2)):int(M-(i*M/(k*2)))]).flatten()))
        pixels=np.concatenate((pixels,(fshift[int(i*O/(k*2)):int((i+1)*O/(k*2)),int((i+1)*N/(k*2)):int(N-((i+1)*N/(k*2))),int((i+1)*M/(k*2)):int(M-((i+1)*M/(k*2)))]).flatten()))
        pixels=np.concatenate((pixels,(fshift[int(O-((i+1)*O/(k*2))):int(O-(i*O/(k*2))),int((i+1)*N/(k*2)):int(N-((i+1)*N/(k*2))),int((i+1)*M/(k*2)):int(M-((i+1)*M/(k*2)))]).flatten()))
        
    
        regions[i]=pixels
        
    values= np.zeros((k))
    columns=[]
    
    total_magnitude=np.sum(fshift)
    
    for i in range(k):
        values[i]=np.sum(regions[i])/total_magnitude
        columns.append("Frequency_"+str(i))
        
    return columns,values
    
    
if __name__ == "__main__":
    
      #Choose path to the image
      filepath = 'D:/vania'
   
      filename='P72832181_Macular Cube 512x128_2-21-2017_15-41-12_OS_sn5510_cube_raw'
    
    
      fname = filepath+'/'+filename+'.img'
      fname_layers = filepath+'/'+filename+'_Surfaces_Iowa.xml'
      data_oct = oct.OctLayers(filename=fname_layers,
                                 raw_filename=fname)
      
      mask=np.load(filepath+'/'+filename+'_octmask.npy')
        

      layers=["RNFL", "GCL","IPL","INL","OPL","ONL","IS/OS","OuterSegment","OPR", "RPE"]
     
      columns=[]
      values=[]
      V=data_oct.octdata
      
      for i in range(1):#len(layers)):
        volume=V.copy()
        volume[mask!=i+1]=0
        c, v=fourierFeatures3D(volume,4)
        columns+=[j+'_'+layers[i] for j in c]
        values+=v.tolist()
        
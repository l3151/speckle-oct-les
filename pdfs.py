# -*- coding: utf-8 -*-
"""
Created on Sat Apr 10 14:48:11 2021

@author: vania - - University of Coimbra / Eramus MC
"""

import numpy as np
import scipy.stats as st
import App.OctLayers as oct


def pdfs(data_oct):
    """
    Fit different distributions to data_oct and return 
    the feature values for each parameter distribution

    Parameters
    ----------
    data_oct : matrix/vector with OCT data

    Returns
    -------
    columns : list of column names
    values : list of feature values

    """

    DISTRIBUTIONS= [
            st.gamma,st.gengamma,st.kappa4,
            st.lognorm,st.nakagami,
            st.rayleigh,st.rice,
            st.weibull_min
        ]
    
    #List that will contain each feature value and columns name
    values=[]
    columns=[]
    
    #Create features and column names
    for distribution in DISTRIBUTIONS:
        # fit dist to data
        v=distribution.fit(data_oct)
        values+=v
        for i in range(len(v)):
            columns.append(distribution.name+'_'+str(i+1))
        
    return columns, values


# Speckle OCT LES

## Description

This repository provides the Python functions used to compute OCT speckle features on OCT volumes. Please for any additional detailes please refer to the manuscript in submission process:

## Functions

createFeatures.py - Main file which loads the OCT valumes and masks.

Individual functions for features computation.
contrastMap.py
fourierDomain.py
pdfs.py
SGLDM.py

Complete set of speckle features computed on subset of LES cohort.
Tickness_severity.csv
speckle_features.csv

Sample folder includes an OCT volume and the respective segmenation.

